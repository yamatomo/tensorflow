# TensorFlow

## 必要なもの

- そこそこのスペックのPC推奨
- マナカナを見分ける能力
- [ユイメタルモアメタルを見分ける能力](http://blog.livedoor.jp/bbmt46/archives/31728982.html)

![マナカナ](desc/EALIfH3UEAAhnFG.jpg)
![ユイメタルモアメタル](desc/GEmsujlk2u0Zxyngw25wo4ocMpauAjv17zt04PDU2_s.png)
![ユイメタルモアメタル2](desc/D-jzQFkUYAIvCl1.jpg)

```
ゆいにみえるほうがもあ 
もあにみえるほうがゆい
```

## 機械学習とは

機械学習（きかいがくしゅう、（英: Machine learning、略称: ML）は、明示的な指示を用いることなく、その代わりにパターンと推論に依存して、  
特定の課題を効率的に実行するためにコンピュータシステムが使用するアルゴリズムおよび統計モデルの科学研究である。  
([Wikipedia](https://ja.wikipedia.org/wiki/%E6%A9%9F%E6%A2%B0%E5%AD%A6%E7%BF%92)より)  

## 機械学習のアルゴリズムの分類

- 教師あり学習  
  入力とそれに対応すべき出力（人間の専門家が訓練例にラベル付けすることで提供されることが多いのでラベルとも呼ばれる）を写像する関数を生成する。例えば、分類問題では入力ベクトルと出力に対応する分類で示される例を与えられ、それらを写像する関数を近似的に求める。
- 教師なし学習  
  入力のみ（ラベルなしの例）からモデルを構築する。データマイニングも参照。
- 半教師あり学習  
  ラベルありの例とラベルなしの例をどちらも扱えるようにしたもので、それによって近似関数または分類器を生成する。
- 強化学習  
  周囲の環境を観測することでどう行動すべきかを学習する。行動によって必ず環境に影響を及ぼし、環境から報酬という形でフィードバックを得ることで学習アルゴリズムのガイドとする。例えばQ学習がある。
- トランスダクション（トランスダクティブ推論）  
  観測された具体的な（訓練）例から具体的かつ固定の（テスト）例の新たな出力を予測しようとする。
- マルチタスク学習  
  関連する複数の問題について同時に学習させ、主要な問題の予測精度を向上させる。
([Wikipedia](https://ja.wikipedia.org/wiki/%E6%A9%9F%E6%A2%B0%E5%AD%A6%E7%BF%92#%E3%82%A2%E3%83%AB%E3%82%B4%E3%83%AA%E3%82%BA%E3%83%A0%E3%81%AE%E5%88%86%E9%A1%9E)より)  

## TensorFlowとは

TensorFlowとは、Googleが開発しオープンソースで公開している、機械学習に用いるためのソフトウェアライブラリである。  

## 環境設定（Mac）

- homebrew インストール  
- pip(Python パッケージマネージャ) から tensorflow インストール(1敗)  
  クソ雑魚PCユーザーはcondaでインストールしましょう[^1]  
  クソ雑魚PCで調子に乗ってpipでインストールすると、  
  TensorFlow 実行テストではまります。  
- conda インストール  
- tensorflow 環境構築  

## 環境設定（Win）

### Miniconda インストール

https://docs.conda.io/en/latest/miniconda.html  

### TensorFlow 環境構築

Start＞Anaconda3(64-bit)＞Anaconda Prompt を開く

```
NG (base)> conda create --name=tensorenv python=3.7 tensorflow tensorflow_hub tensorboard
OK (base)> conda create --name=tensorenv python=3.7 tensorflow tensorboard
```

#### TensorFlow 環境にスイッチ

```
(base)> activate tensorenv
```

#### tensorflow_hub インストール

```
(tensorenv)> conda install -c conda-forge tensorflow-hub
```

## TensorFlow 実行テスト

```
(tensorenv)> python -c "import tensorflow as tf;print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
```

※下記エラーが出た場合はお帰りください[^1]  
```
Illegal instruction: 4
```

## tensorflow_hub から必要なものをダウンロード

### download 学習スクリプト

```
$ curl -O https://raw.githubusercontent.com/tensorflow/hub/master/examples/image_retraining/retrain.py
```

### download 判別スクリプト

```
$ curl -O https://raw.githubusercontent.com/tensorflow/tensorflow/master/tensorflow/examples/label_image/label_image.py
```

## 画像収集

ポンデリングとブラックホールの画像を集める  

## 学習

```
$ python retrain.py --image_dir images00 --how_many_training_steps 1000
```

```
$ tensorboard --logdir /tmp/retrain_logs
```

### [ZeroDivisionError] (´・ω・｀)[^2]  

```
CRITICAL:tensorflow:Label blackhole has no images in the category validation.
E0710 06:22:09.553891 140735980557184 retrain.py:259] CRITICAL - Label blackhole has no images in the category validation.
Traceback (most recent call last):
  File "retrain.py", line 1349, in <module>
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
  File "/.pyenv/versions/anaconda3-2019.03/envs/tensorflow/lib/python3.6/site-packages/tensorflow/python/platform/app.py", line 125, in run
    _sys.exit(main(argv))
  File "retrain.py", line 1115, in main
    FLAGS.tfhub_module))
  File "retrain.py", line 519, in get_random_cached_bottlenecks
    image_dir, category)
  File "retrain.py", line 260, in get_image_path
    mod_index = index % len(category_list)
ZeroDivisionError: integer division or modulo by zero
```

```
W0710 06:34:57.627802 140735980557184 retrain.py:191] WARNING: Folder has less than 20 images, which may cause issues.
```

かいてる通り２０枚以上にする。  
それでも解決しない場合は問題ありそうな画像をフィーリングで省いてみる  

### [OMP: Error #15] なんこれ・・・[^3]

```
OMP: Error #15: Initializing libiomp5.dylib, but found libiomp5.dylib already initialized.
OMP: Hint This means that multiple copies of the OpenMP runtime have been linked into the program. That is dangerous, since it can degrade performance or cause incorrect results. The best thing to do is to ensure that only a single OpenMP runtime is linked into the process, e.g. by avoiding static linking of the OpenMP runtime in any library. As an unsafe, unsupported, undocumented workaround you can set the environment variable KMP_DUPLICATE_LIB_OK=TRUE to allow the program to continue to execute, but that may cause crashes or silently produce incorrect results. For more information, please see http://www.intel.com/software/products/support/.
Abort trap: 6
```

```
$ vim retain.py

import os↲
os.environ['KMP_DUPLICATE_LIB_OK']='TRUE'↲
```

上記を追加すれば通る・・・  

## 判別

```
$ python label_image.py \
--graph /tmp/output_graph.pb --labels /tmp/output_labels.txt \
--input_layer Placeholder \
--output_layer final_result \
--image input.jpg
```

```
$ python label_image.py --graph model/images00_output_graph.pb --labels model/images00_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/black_test00.jpg
blackhole 0.9991831
pondering 0.00081681204

$ python label_image.py --graph model/images00_output_graph.pb --labels model/images00_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/black_test01.jpg
blackhole 0.9932468
pondering 0.0067531657

$ python label_image.py --graph model/images00_output_graph.pb --labels model/images00_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/ponde_test00.jpg
pondering 0.9989279
blackhole 0.0010720314

$ python label_image.py --graph model/images00_output_graph.pb --labels model/images00_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/ponde_test01.jpg
pondering 0.9987086
blackhole 0.00129143
```

Win用
```
> python label_image.py --graph c:\tmp\output_graph.pb --labels c:\tmp\output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test\black_test00.jpg
```

## オニオンリングも混ぜて学習

```
> xcopy images00_omake\onion images00\onion\
> python retrain.py --image_dir images00 --how_many_training_steps 1000
```

## オニオンリング混みの学習データで判別

```
$ python label_image.py --graph model/images00_omake_output_graph.pb --labels model/images00_omake_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/black_test00.jpg
blackhole 0.9986541
pondering 0.0010404115
onion 0.00030547302

$ python label_image.py --graph model/images00_omake_output_graph.pb --labels model/images00_omake_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/black_test01.jpg
blackhole 0.990831
pondering 0.008521932
onion 0.0006470062

$ python label_image.py --graph model/images00_omake_output_graph.pb --labels model/images00_omake_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/ponde_test00.jpg
pondering 0.99299556
onion 0.005624423
blackhole 0.0013800499

$ python label_image.py --graph model/images00_omake_output_graph.pb --labels model/images00_omake_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/ponde_test01.jpg
pondering 0.9681981
onion 0.02969758
blackhole 0.002104345

$ python label_image.py --graph model/images00_omake_output_graph.pb --labels model/images00_omake_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/onion_1200.jpg
onion 0.95164716
pondering 0.042466614
blackhole 0.0058861948

$ python label_image.py --graph model/images00_omake_output_graph.pb --labels model/images00_omake_output_labels.txt --input_layer Placeholder --output_layer final_result --image images00_test/ONION_RING.jpg
onion 0.9348462
pondering 0.057298925
blackhole 0.007854796
```

## プチ解説

- 学習に使った画像について
- イカリングについて

## おまけ

### マナカナ判別

```
$ python label_image.py --graph model/images01_output_graph.pb --labels model/images01_output_labels.txt --input_layer Placeholder --output_layer final_result --image images01_test/mana00.jpg
mana 0.95291674
kana 0.04708321

$ python label_image.py --graph model/images01_output_graph.pb --labels model/images01_output_labels.txt --input_layer Placeholder --output_layer final_result --image images01_test/mana01.jpg
mana 0.63967544
kana 0.3603245

$ python label_image.py --graph model/images01_output_graph.pb --labels model/images01_output_labels.txt --input_layer Placeholder --output_layer final_result --image images01_test/kana00.jpg
kana 0.94782096
mana 0.052179083

$ python label_image.py --graph model/images01_output_graph.pb --labels model/images01_output_labels.txt --input_layer Placeholder --output_layer final_result --image images01_test/kana01.jpg
kana 0.98239094
mana 0.017609075
```

### ユイモア判別

```
$ python label_image.py --graph model/images02_output_graph.pb --labels model/images02_output_labels.txt --input_layer Placeholder --output_layer final_result --image images02_test/yui/mig.jpg
moa 0.8536791
yui 0.14632088

$ python label_image.py --graph model/images02_output_graph.pb --labels model/images02_output_labels.txt --input_layer Placeholder --output_layer final_result --image images02_test/moa/D-j2pDbU8AAE3xO.jpg
moa 0.8955605
yui 0.1044395

$ python label_image.py --graph model/images02_output_graph.pb --labels model/images02_output_labels.txt --input_layer Placeholder --output_layer final_result --image images02_test/moa/D-lOLq6VAAIMAna.jpg
yui 0.768549
moa 0.23145096

$ python label_image.py --graph model/images02_output_graph.pb --labels model/images02_output_labels.txt --input_layer Placeholder --output_layer final_result --image images02_test/moa/D-Uov1KUcAAtvzK.jpg
moa 0.5648788
yui 0.43512118
```

## トラブルシューティング
[^1]:pip の tensorflow はAVX対応CPUでないとダメです。おとなしくcondaでインストールしましょう。
[^2]:画像枚数が少ないとダメです。20枚以上にしましょう。  
  https://stackoverflow.com/questions/38175673/critical-tensorflowcategory-has-no-images-validation
[^3]:謎
